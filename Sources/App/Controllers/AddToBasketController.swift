//
//  AddToBasketController.swift
//  
//
//  Created by Lewis on 25.01.2022.
//

import Foundation
import Vapor

class AddToBasketController {
    func addToBasket(_ req: Request) throws -> EventLoopFuture<BasketResponse> {
        guard let body = try? req.content.decode(AddToBasketRequest.self) else {
            throw Abort(.badRequest)
        }
        
        print(body)
        
        let response = BasketResponse(result: 1,
                                      message: "Товар успешно добавлен")
        
        return req.eventLoop.future(response)
    }
}


