//
//  PayBasketController.swift
//  
//
//  Created by Lewis on 26.01.2022.
//

import Foundation
import Vapor

class PayBasketController {
    func payBasket(_ req: Request) throws -> EventLoopFuture<BasketResponse> {
        guard let body = try? req.content.decode(PayBasketRequest.self) else {
            throw Abort(.badRequest)
        }
        
        print(body)
        
        let response = BasketResponse(result: 1,
                                      message: "Покупка прошла успешно!")
        
        return req.eventLoop.future(response)
    }
}
