//
//  DeleteFromBasketController.swift
//  
//
//  Created by Lewis on 25.01.2022.
//

import Foundation
import Vapor

class DeleteFromBasketController {
    func deleteFromBasket(_ req: Request) throws -> EventLoopFuture<BasketResponse> {
        guard let body = try? req.content.decode(DeleteFromBasketRequest.self) else {
            throw Abort(.badRequest)
        }
        
        print(body)
        
        let response = BasketResponse(result: 1,
                                      message: "Товар успешно удалён")
        
        return req.eventLoop.future(response)
    }
}
