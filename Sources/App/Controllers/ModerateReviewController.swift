//
//  ModerateReviewController.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

class ModerateReviewController {
    func moderateReview(_ req: Request) throws -> EventLoopFuture<RegisterResponse> {
        guard let body = try? req.content.decode(DeleteReviewRequest.self) else {
            throw Abort(.badRequest)
        }
        
        print(body)
        
        let response = RegisterResponse(
            result: 1,
            user_message: nil,
            error_message: nil
        )
        
        return req.eventLoop.future(response)
    }
}
