//
//  ReviewsController.swift
//  
//
//  Created by Lewis on 26.02.2022.
//

import Foundation
import Vapor

class ReviewsController {
    func getReviews(_ req: Request) throws -> EventLoopFuture<GetReviewsResponse> {
        guard let body = try? req.content.decode(GetReviewsRequest.self) else {
            throw Abort(.badRequest)
        }
        
        print(body)
        
        let response = GetReviewsResponse(reviews: [Review(comment_id: 1, author_name: "Bill Gates", review_text: "Good!!!"),
                                                    Review(comment_id: 2, author_name: "Tim just Tim", review_text: "Apple is the best!")
                                                   ])
        
        return req.eventLoop.future(response)
    }
}
