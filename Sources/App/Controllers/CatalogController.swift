//
//  CatalogController.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

class CatalogController {
    func catalog(_ req: Request) throws -> EventLoopFuture<CatalogResponse> {
        guard let body = try? req.content.decode(CatalogRequest.self) else {
            throw Abort(.badRequest)
        }
        
        print(body)
        
        let response = CatalogResponse(page_number: 1,
                                       products: [Product(id_product: 1,
                                                          product_name: "Продукт 1",
                                                          price: 111),
                                                  Product(id_product: 2,
                                                          product_name: "Продукт 2",
                                                          price: 222)
                                                 ])
        
        return req.eventLoop.future(response)
    }
}

