//
//  ProductController.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

class ProductController {
    func product(_ req: Request) throws -> EventLoopFuture<ProductResponse> {
        guard let body = try? req.content.decode(ProductRequest.self) else {
            throw Abort(.badRequest)
        }
        
        print(body)
        
        let response = ProductResponse(result: 1,
                                       product_name: "Название",
                                       product_price: 123,
                                       product_description: "Описание")
        
        return req.eventLoop.future(response)
    }
}

