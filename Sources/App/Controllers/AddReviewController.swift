//
//  AddReviewController.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

class AddReviewController {
    func addReview(_ req: Request) throws -> EventLoopFuture<RegisterResponse> {
        guard let body = try? req.content.decode(AddReviewRequest.self) else {
            throw Abort(.badRequest)
        }
        
        print(body)
        
        let response = RegisterResponse(
            result: 1,
            user_message: "Ваш отзыв был передан на модерацию",
            error_message: nil
        )
        
        return req.eventLoop.future(response)
    }
}
