//
//  LogInController.swift
//  
//
//  Created by Lewis on 22.01.2022.
//

import Foundation
import Vapor

class LogInController {
    func logIn(_ req: Request) throws -> EventLoopFuture<LogInResponse> {
        guard let body = try? req.content.decode(LogInRequest.self) else {
            throw Abort(.badRequest)
        }
        
        print(body)
        
        let response = LogInResponse(result: 1,
                                     user: User(id_user: 123,
                                                user_login: "geekbrains",
                                                user_name: "Steve",
                                                user_lastname: "Jobs"),
                                     profile: Profile(username: "JOBSS",
                                                      password: "12345",
                                                      email: "@mail@mail@",
                                                      gender: "m",
                                                      credit_card: "1234567890",
                                                      bio: "Best of the best")
        )
        
        return req.eventLoop.future(response)
    }
}


