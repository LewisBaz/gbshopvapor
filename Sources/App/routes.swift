import Vapor

func routes(_ app: Application) throws {
    app.get { req in
        return "It works!"
    }

    app.get("hello") { req -> String in
        return "Hello, world!"
    }
    
    let authController = AuthController()
    app.post("register", use: authController.register)
    
    let logInController = LogInController()
    app.post("login", use: logInController.logIn)
    
    let logOutController = LogOutController()
    app.post("logout", use: logOutController.logOut)
    
    let changeDataController = ChangeDataController()
    app.post("changedata", use: changeDataController.changeData)
    
    let catalogController = CatalogController()
    app.post("catalog", use: catalogController.catalog)
    
    let productController = ProductController()
    app.post("productByID", use: productController.product)
    
    let addReviewController = AddReviewController()
    app.post("addReview", use: addReviewController.addReview)
    
    let moderateReviewController = ModerateReviewController()
    app.post("moderateReview", use: moderateReviewController.moderateReview)
    
    let deleteReviewController = DeleteReviewController()
    app.post("deleteReview", use: deleteReviewController.deleteReview)
    
    let addToBasketController = AddToBasketController()
    app.post("addToBasket", use: addToBasketController.addToBasket)
    
    let deleteFromBasketController = DeleteFromBasketController()
    app.post("deleteFromBasket", use: deleteFromBasketController.deleteFromBasket)
    
    let payBasketController = PayBasketController()
    app.post("payBasket", use: payBasketController.payBasket)
    
    let getReviewsController = ReviewsController()
    app.post("getReviews", use: getReviewsController.getReviews)
}
