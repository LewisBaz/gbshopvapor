//
//  User.swift
//  
//
//  Created by Lewis on 22.01.2022.
//

import Foundation
import Vapor

struct User: Content {
    var id_user: Int
    var user_login: String
    var user_name: String
    var user_lastname: String
}
