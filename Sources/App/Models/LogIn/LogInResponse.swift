//
//  LogInResponse.swift
//  
//
//  Created by Lewis on 22.01.2022.
//

import Foundation
import Vapor

struct LogInResponse: Content {
    var result: Int
    var user: User
    var profile: Profile
}
