//
//  LogInRequest.swift
//  
//
//  Created by Lewis on 22.01.2022.
//

import Foundation
import Vapor

struct LogInRequest: Content {
    var username: String
    var password: String
}
