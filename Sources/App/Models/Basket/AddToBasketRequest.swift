//
//  AddToBasketRequest.swift
//  
//
//  Created by Lewis on 25.01.2022.
//

import Foundation
import Vapor

struct AddToBasketRequest: Content {
    var id_product: Int
    var quantity: Int
}
