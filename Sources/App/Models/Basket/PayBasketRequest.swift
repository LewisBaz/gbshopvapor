//
//  PayBasketRequest.swift
//  
//
//  Created by Lewis on 26.01.2022.
//

import Foundation
import Vapor

struct PayBasketRequest: Content {
    var id_user: Int
    var amount: Int
}
