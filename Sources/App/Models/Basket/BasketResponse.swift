//
//  BasketResponse.swift
//  
//
//  Created by Lewis on 25.01.2022.
//

import Foundation
import Vapor

struct BasketResponse: Content {
    var result: Int
    var message: String
}
