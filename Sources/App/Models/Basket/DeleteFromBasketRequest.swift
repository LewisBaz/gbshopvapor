//
//  DeleteFromBasketRequest.swift
//  
//
//  Created by Lewis on 25.01.2022.
//

import Foundation
import Vapor

struct DeleteFromBasketRequest: Content {
    var id_product: Int
}

