//
//  AddReviewRequest.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

struct AddReviewRequest: Content {
    var id_user: Int
    var text: String
}
