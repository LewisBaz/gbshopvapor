//
//  GetReviewsRequest.swift
//  
//
//  Created by Lewis on 26.02.2022.
//

import Foundation
import Vapor

struct GetReviewsRequest: Content {
    var id_product: Int
}
