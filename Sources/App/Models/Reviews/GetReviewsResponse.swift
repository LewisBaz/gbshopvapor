//
//  GetReviewsResponse.swift
//  
//
//  Created by Lewis on 26.02.2022.
//

import Foundation
import Vapor

struct GetReviewsResponse: Content {
    var reviews: [Review]
}


