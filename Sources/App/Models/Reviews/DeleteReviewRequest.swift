//
//  DeleteReviewRequest.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

struct DeleteReviewRequest: Content {
    var id_comment: Int
}
