//
//  CatalogResponse.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

struct CatalogResponse: Content {
    var page_number: Int
    var products: [Product]
}
