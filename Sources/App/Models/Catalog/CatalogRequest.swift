//
//  CatalogRequest.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

struct CatalogRequest: Content {
    var page_number: Int
    var id_category: Int
}
