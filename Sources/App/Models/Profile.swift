//
//  Profile.swift
//  
//
//  Created by Lewis on 08.02.2022.
//

import Vapor

struct Profile: Content {
    var username: String
    var password: String
    var email: String
    var gender: String
    var credit_card: String
    var bio: String
}
