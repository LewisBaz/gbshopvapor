//
//  ProductResponse.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

struct ProductResponse: Content {
    var result: Int
    var product_name: String
    var product_price: Int
    var product_description: String
}
