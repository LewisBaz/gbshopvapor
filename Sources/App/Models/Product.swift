//
//  Product.swift
//  
//
//  Created by Lewis on 23.01.2022.
//

import Foundation
import Vapor

struct Product: Content {
    var id_product: Int
    var product_name: String
    var price: Int
}
