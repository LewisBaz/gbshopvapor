//
//  LogOutRequest.swift
//  
//
//  Created by Lewis on 22.01.2022.
//

import Foundation
import Vapor

struct LogOutRequest: Content {
    var id_user: Int
}
