//
//  Review.swift
//  
//
//  Created by Lewis on 26.02.2022.
//

import Foundation
import Vapor

struct Review: Content {
    var comment_id: Int
    var author_name: String
    var review_text: String
}
